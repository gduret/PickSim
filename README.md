# PickSim

PickSim is a gazebo-based pipeline to generate synthetic data. 

## Getting started

Picksim can easily be downloaded and tested trought a docker container at the link : https://hub.docker.com/repository/docker/guillaume0477/picksim/general or with the command "docker pull guillaume0477/picksim:v0.1" . 

To facilitate your initiation with PickSim, here is a list of recommended steps to generate example data for the FruitBin dataset.

## Run the docker containner 

To run the docker container : 
```
docker run -it --rm --device /dev/dri:/dev/dri --user gduret --gpus all -e DISPLAY=$DISPLAY -e NVIDIA_VISIBLE_DEVICES=all -e NVIDIA_DRIVER_CAPABILITIES=all --mount type=bind,source=/home/gduret/dataset_host,target=/dataset  -v /tmp/.X11-unix:/tmp/.X11-unix:rw guillaume0477/picksim:v0.1
```

## Preprocess 

The first command command is used to download assets of fruits
```
roslaunch dataset install.launch name:=objects version:=default
```

The second command is used to process the raw meshes to get sdf files for the gazebo simaultion until the statement : "[s2rg/blender-1] process has finished cleanly". 
```
roslaunch dataset generate.launch name:=objects version:=optimized k_loop:=0
```

The following command is used to randomize the scene and generate at the last step world files of each scenes with all different lights. The statement "[s2rg/blender-1] process has finished cleanly" allow to be sure that everithing has done successfully. 
```
roslaunch dataset generate.launch name:=s2rg version:=Fruits_all_medium k_loop:=0

```

You can modify the file "/opt/learn-real/ros/melodic/integration/src/s2rg/config/version/generator/Fruits_all_medium.yaml" to change the number of scenes generated.


## Recording

The record command uses the pregenerated world file to record the camera data for the scenes 1 to 5
```
roslaunch dataset record.launch name:=s2rg version:=Fruits_all_medium begin:="0" end:="5"
```
It tould take some time to generate until the program reach the stateement : [s2rg/recorder-2] process has finished cleanly

For more customization, you can modify the file "/opt/learn-real/ros/melodic/integration/src/s2rg/config/recorder.yaml". This file allows you to change the timing of the recording process and specify which features should be recorded. By adjusting the settings in this file, you can control the number of images recorded in each scene.


## Visualization

To visualize the dataset in rviz the command is : 
```
roslaunch dataset read.launch name:=s2rg version:=Fruits_all_medium
```

## Other scenes


Fruits_all_medium can be replaced to get other scene arangement as : "Fruits_apple_bin.yaml", "Fruits_apricot_bin.yaml", "Fruits_banana_bin.yaml", "Fruits_cherry_bin.yaml", "Fruits_kiwi_bin.yaml", "Fruits_lemon_bin.yaml", "Fruits_orange_bin.yaml", "Fruits_peach_bin.yaml", "Fruits_pear_bin.yaml"

Scenes with ycb objects can be generated with the commands : 

```
roslaunch dataset install.launch name:=ycb version:=retopology
roslaunch dataset generate.launch name:=ycb version:=standard k_loop:=0
roslaunch dataset generate.launch name:=s2rg version:=ycb_retopology k_loop:=0
roslaunch dataset record.launch name:=s2rg version:=ycb_retopology begin:="0" end:="5"
```


## Roadmap

PickSim is now accessible via Docker, making it easier to install and test. The source files will be added to this repository as soon as possible.


